let fs = require("fs");

function createDirectory(pathName) {
  console.log("Creation of Directory Starts -");
  let promiseForCreationOfDirectory = new Promise((resolve, reject) => {
    fs.mkdir(pathName, { recursive: true }, (errorGot) => {
      if (errorGot) {
        reject(errorGot);
      } else {
        console.log("Directory Created");
        resolve("Promise Fullfilled - Creation of Directory SuccessFull");
      }
    });
  });
  return promiseForCreationOfDirectory;
}

function createRandomFiles(pathName, arrayPassed) {
  console.log("Creation of Files Starts -");
  let promiseForCreationOfFiles = new Promise((resolve, reject) => {
    let numberPassed = 1;
    function recursiveCreation(numberPassed) {
      let objectCreated = {
        Name: "RandomName" + numberPassed,
        Value: "RandomValue" + numberPassed,
      };
      let jsonString = JSON.stringify(objectCreated);
      let filePath = pathName + "/Random_JSONFile" + numberPassed + ".json";
      return new Promise((resolveWrite, rejectWrite) => {
        if (numberPassed === arrayPassed.length + 1) {
          resolveWrite();
        } else {
          fs.writeFile(filePath, jsonString, (errorGot) => {
            if (errorGot) {
              console.error("Error for Creation of Files:");
              rejectWrite(errorGot);
            } else {
              console.log("File Created");
              recursiveCreation(numberPassed + 1)
                .then(() => {
                  resolveWrite();
                })
                .catch(() => {
                  rejectWrite();
                });
            }
          });
        }
      });
    }
    return new Promise((resolveRecursive, rejectRecursive) => {
      recursiveCreation(numberPassed)
        .then(() => {
          resolveRecursive();
        })
        .catch(() => {
          rejectRecursive();
        });
    })
      .then(() => {
        resolve("Promise FullFilled - Creation of File Successfull");
      })
      .catch(() => {
        reject("Promise Broken - Creation of File Successfull");
      });
  });
  return promiseForCreationOfFiles;
}

function deleteAllFiles(pathName, arrayPassed) {
  console.log("Deletion of Files Starts -");
  let promiseForDeletionOfFiles = new Promise((resolve, reject) => {
    let numberPassed = 1;
    function recursiveDeletion(numberPassed) {
      let filePath = pathName + "/Random_JSONFile" + numberPassed + ".json";
      return new Promise((resolveDeletion, rejectDeletion) => {
        if (numberPassed === arrayPassed.length + 1) {
          resolveDeletion();
        } else {
          fs.unlink(filePath, (errorGot) => {
            if (errorGot) {
              console.error("Error for Deletion of Files:");
              rejectDeletion(errorGot);
            } else {
              console.log("File Deleted");
              recursiveDeletion(numberPassed + 1)
                .then(() => {
                  resolveDeletion();
                })
                .catch(() => {
                  rejectDeletion();
                });
            }
          });
        }
      });
    }
    return new Promise((resolveRecursive, rejectRecursive) => {
      recursiveDeletion(numberPassed)
        .then(() => {
          resolveRecursive();
        })
        .catch(() => {
          rejectRecursive();
        });
    })
      .then(() => {
        resolve("Promise FullFilled - Deletion of File Successfull");
      })
      .catch(() => {
        reject("Promise Broken - Deletion of File Successfull");
      });
  });
  return promiseForDeletionOfFiles;
}

module.exports = { createDirectory, createRandomFiles, deleteAllFiles };
