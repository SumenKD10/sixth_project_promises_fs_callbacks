let {
  createDirectory,
  createRandomFiles,
  deleteAllFiles,
} = require("../problem1.cjs");

//Creation of Random Number
let randomNumber = Math.floor(Math.random() * (20 - 1) + 1);
console.log("Random Number Generated :" + randomNumber + "\n");

//Creation of an array of consecutive integers
let count = 0;
let arrayFormed = Array(randomNumber)
  .fill(0)
  .map((index) => {
    index = ++count;
    return index;
  });

//Forming a variable to store the directory path
let pathName = __dirname + "/../Random_JSON_Directory";

//Chaining of Promises
createDirectory(pathName)
  .then(function (message) {
    console.log(message + "\n");
    return createRandomFiles(pathName, arrayFormed);
  })
  .then(function (message) {
    console.log(message + "\n");
    return deleteAllFiles(pathName, arrayFormed);
  })
  .then(function (message) {
    console.log(message + "\n");
  })
  .catch(function (err) {
    console.error(err);
  });
