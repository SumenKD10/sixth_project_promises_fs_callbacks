const {
  readTheFile,
  convertMakeAndStoreUpperCase,
  lowerCaseSentence,
  sortedFile,
  deleteAllFiles,
} = require("../problem2.cjs");

//Chaining of Promises
readTheFile()
  .then((message) => {
    console.log(message + "\n");
    return convertMakeAndStoreUpperCase();
  })
  .then((message) => {
    console.log(message + "\n");
    return lowerCaseSentence();
  })
  .then((message) => {
    console.log(message + "\n");
    return sortedFile();
  })
  .then((message) => {
    console.log(message + "\n");
    return deleteAllFiles();
  })
  .then(function (message) {
    console.log(message + "\n");
  })
  .then(function () {
    console.log("Operation Successfull.");
  })
  .catch((errorGot) => {
    console.error(errorGot);
  });
