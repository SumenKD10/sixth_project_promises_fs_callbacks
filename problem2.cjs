let fs = require("fs");
let path = require("path");

function readTheFile() {
  console.log("\nReading of File Starts - ");
  let promiseForReadingTheFile = new Promise((resolve, reject) => {
    fs.readFile("lipsum.txt", "utf8", (errorGot, data) => {
      if (errorGot) {
        console.error(errorGot);
        reject("Promise Broken - Error got while Reading the File.");
      } else {
        console.log("File Read Successfully.");
        resolve("Promise Fullfilled - File Read Successfully.");
      }
    });
  });
  return promiseForReadingTheFile;
}

function convertMakeAndStoreUpperCase() {
  console.log("Converting into Upper Case of File Starts - ");
  let promiseToConvertUpper = new Promise((resolveFinal, rejectFinal) => {
    fs.readFile("lipsum.txt", "utf8", (errorGot1, data) => {
      if (errorGot1) {
        console.error("Error:");
        console.error(errorGot1);
        rejectFinal("Promise Broken - Error got while converting to Upper");
      } else {
        let dataConverted = data.toUpperCase();
        let upperFileName = "newUpperCaseFile.txt";
        let filePath = __dirname + "/" + upperFileName;
        return new Promise((resolveWrite, rejectWrite) => {
          fs.writeFile(filePath, dataConverted, (errorGot2) => {
            if (errorGot2) {
              console.error("Error:");
              console.error(errorGot2);
              rejectWrite(
                "Promise Broken - Error got while converting to Upper"
              );
            } else {
              console.log("Upper Case File Created SuccessFully");
              return new Promise((resolveAppend, rejectAppend) => {
                fs.appendFile(
                  "filenames.txt",
                  upperFileName + "\n",
                  (errorGot3) => {
                    if (errorGot3) {
                      rejectAppend(
                        "Promise Broken - Error got while converting to Upper"
                      );
                    } else {
                      resolveAppend();
                    }
                  }
                );
              })
                .then(() => {
                  resolveWrite();
                })
                .catch(() => {
                  rejectWrite();
                });
            }
          });
        })
          .then(() => {
            resolveFinal(
              "Promise Fullfilled - File Converted to Upper Case Successfully"
            );
          })
          .catch(() => {
            rejectFinal(
              "Promise Broken - File Converted to Upper Case Successfully"
            );
          });
      }
    });
  });
  return promiseToConvertUpper;
}

function lowerCaseSentence() {
  console.log("Converting into Lower Case of File Starts - ");
  let promiseToConvertLower = new Promise((resolveFinal, rejectFinal) => {
    fs.readFile(
      path.join(__dirname, "newUpperCaseFile.txt"),
      "utf8",
      (errorGot1, data) => {
        if (errorGot1) {
          console.error("Error:");
          console.error(errorGot1);
          rejectFinal(
            "Promise Broken - Error Got while Converting to Lower Case"
          );
        } else {
          let lowerFileName = "newLowerCaseFile.txt";
          let newData = data.toLowerCase().split(". ").join(".\n");
          return new Promise((resolveAppend, rejectAppend) => {
            fs.appendFile(lowerFileName, newData, (errorGot) => {
              if (errorGot) {
                console.error("Error:");
                console.error(errorGot);
                rejectAppend(
                  "Promise Broken - Error Got while Converting to Lower Case"
                );
              } else {
                console.log("Lower Case File Created SuccessFully");
                return new Promise(
                  (resolveAppendInside, rejectAppendInside) => {
                    fs.appendFile(
                      "filenames.txt",
                      lowerFileName + "\n",
                      (errorGotNew) => {
                        if (errorGotNew) {
                          rejectAppendInside(
                            "Promise Broken - Error Got while Converting to Lower Case"
                          );
                        } else {
                          resolveAppendInside();
                        }
                      }
                    );
                  }
                )
                  .then(() => {
                    resolveAppend();
                  })
                  .catch(() => {
                    rejectAppend();
                  });
              }
            });
          })
            .then(() => {
              resolveFinal(
                "Promise Fullfilled - File Converted to Lower Case Successfully"
              );
            })
            .catch(() => {
              rejectFinal(
                "Promise Broken - File Converted to Lower Case Successfully"
              );
            });
        }
      }
    );
  });
  return promiseToConvertLower;
}

function sortedFile() {
  console.log("Sorting of File Starts - ");
  let promiseToSort = new Promise((resolveFinal, rejectFinal) => {
    fs.readFile(
      path.join(__dirname, "newLowerCaseFile.txt"),
      "utf8",
      (errorGot, data) => {
        if (errorGot) {
          console.error("Error:");
          console.error(errorGot);
          rejectFinal("Error while Sorting the File");
        } else {
          let newData = data.split("\n").sort();
          let sortedFileName = "sortedFile.txt";
          return new Promise((resolveAppend, rejectAppend) => {
            fs.appendFile(
              sortedFileName,
              newData.join("\n").toString(),
              (errorGotNew1) => {
                if (errorGotNew1) {
                  console.error("Error:");
                  console.error(errorGotNew1);
                  rejectAppend("Error while Sorting the File");
                } else {
                  console.log("Sorted File Created SuccessFully");
                  return new Promise(
                    (resolveAppendInside, rejectAppendInside) => {
                      fs.appendFile(
                        "filenames.txt",
                        sortedFileName,
                        (errorGotNew2) => {
                          if (errorGotNew2) {
                            rejectAppendInside("Error while Sorting the File");
                          } else {
                            resolveAppendInside();
                          }
                        }
                      );
                    }
                  )
                    .then(() => {
                      resolveAppend();
                    })
                    .catch(() => {
                      rejectAppend();
                    });
                }
              }
            );
          })
            .then(() => {
              resolveFinal(
                "Promise Fullfilled - Sorted File Written Inside filenames.txt"
              );
            })
            .catch(() => {
              resolveFinal(
                "Promise Broken - Sorted File Written Inside filenames.txt"
              );
            });
        }
      }
    );
  });
  return promiseToSort;
}

function deleteAllFiles() {
  console.log("Deletion of File Starts - ");
  let promiseToDeleteTheFile = new Promise((resolveFinal, rejectFinal) => {
    fs.readFile(
      path.join(__dirname, "filenames.txt"),
      "utf8",
      (errorGot, data) => {
        if (errorGot) {
          console.error("Error:");
          console.error(errorGot);
          rejectFinal("Error While Deleting the File");
        } else {
          let countFiles = 0;
          let filenamesData = data.split("\n");
          let totalFilesCount = filenamesData.length;

          //console.log(countFiles);
          function recursiveDeletion(numberPassed) {
            let filePath = __dirname + "/" + filenamesData[numberPassed];
            return new Promise((resolveDeletion, rejectDeletion) => {
              //console.log(filePath);
              fs.unlink(filePath, (errorGotInside) => {
                if (errorGotInside) {
                  rejectDeletion("Error While Deleting the File");
                } else {
                  console.log("File Deleted");
                  recursiveDeletion(numberPassed + 1)
                    .then(() => {
                      resolveDeletion();
                    })
                    .catch(() => {
                      rejectDeletion();
                    });
                }
              });
              if (numberPassed === totalFilesCount) {
                resolveDeletion();
              }
            });
          }
          return new Promise((resolveRecursive, rejectRecursive) => {
            recursiveDeletion(countFiles)
              .then(() => {
                resolveRecursive();
              })
              .catch(() => {
                rejectRecursive();
              });
          })
            .then(() => {
              resolveFinal("Promise FullFilled - Deletion of File Successfull");
            })
            .catch(() => {
              rejectFinal("Promise Broken - Deletion of File Successfull");
            });
        }
      }
    );
  });
  return promiseToDeleteTheFile;
}

//readTheFile();
//convertMakeAndStoreUpperCase();
//lowerCaseSentence();
//sortedFile();
//deleteAllFiles();

module.exports = {
  readTheFile,
  convertMakeAndStoreUpperCase,
  lowerCaseSentence,
  sortedFile,
  deleteAllFiles,
};
